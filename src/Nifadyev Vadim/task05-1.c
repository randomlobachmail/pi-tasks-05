#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <string.h>

#define N 256 
#define M 10 // ����� ����� � �����

struct BOOK
{
	char FIO[N];
	char bookName[N];
	int publicationYear;
};

void putIntoStruct(struct BOOK *bookshelf, FILE *file);
void printOldestAndNewestBooks(struct BOOK *bookshelf);
void printSortedByLastname(struct BOOK *bookshelf);
void chooseFunction(struct BOOK *bookshelf);

int main()
{
	FILE *file;
	file = fopen("D:\\Programming\\Visual Studio\\Struct books\\Info about books.txt", "r");
	if (file == NULL)
	{
		perror("D:\\Programming\\Visual Studio\\Struct books\\Info about books.txt");
		return 1;
	}
	struct BOOK *bookshelf = (struct BOOK*)malloc(M * sizeof(struct BOOK));
	putIntoStruct(bookshelf, file);
	chooseFunction(bookshelf);
	free(bookshelf);
	fclose(file);
	return 0;
}

void putIntoStruct(struct BOOK *bookshelf, FILE *file) // ���������� ��������� ������� �� �����
{
	int i;
	for (i = 0; i < M; i++)
		fscanf(file, "%s %s %d", bookshelf[i].FIO, bookshelf[i].bookName, &(bookshelf[i].publicationYear));
}

void printOldestAndNewestBooks(struct BOOK *bookshelf) // ����� �� ����� ���� � ����� ����� � ����� ������ ������
{
	int i, newest = -1, oldest = -1, oldestPos = 0, newestPos = 0;
	oldest = bookshelf[0].publicationYear;
	for (i = 1; i < M; i++)
	{
		if (bookshelf[i - 1].publicationYear > bookshelf[i].publicationYear) // ���������� ����� ����� �����
		{
			newest = bookshelf[i - 1].publicationYear;
			newestPos = i - 1;
		}
		if (bookshelf[i].publicationYear < oldest)// ���������� ����� ������ ����� 
		{
			oldest = bookshelf[i].publicationYear;
			oldestPos = i;
		}
	}
	printf("\nThe newest book '%s' was written by %s in %d\n", bookshelf[newestPos].bookName, bookshelf[newestPos].FIO, newest);
	printf("The oldest book '%s' was written by %s in %d\n", bookshelf[oldestPos].bookName, bookshelf[oldestPos].FIO, oldest);
}

void printSortedByLastname(struct BOOK *bookshelf) // ����� �� ����� ���� � ������, ������������� �� �������� �������
{
	int i, j, temp;
	int index[M]; // ������ �������� �����
	for (i = 0; i < M; i++)
		index[i] = i;
	for (i = 0; i < M; i++)
	{
		for (j = 0; j < M; j++)
			if (strcmp(bookshelf[index[i]].FIO, bookshelf[index[j]].FIO) < 0)
			{
				temp = index[i];
				index[i] = index[j];
				index[j] = temp;
			}
	}
	printf("\nInfo about books sorted by author's lastname\n");
	for (i = 0; i < M; i++)
		printf("%s - '%s', %d\n", bookshelf[index[i]].FIO, bookshelf[index[i]].bookName, bookshelf[index[i]].publicationYear);
}

void chooseFunction(struct BOOK *bookshelf) // ����� �������� ������ ���� � ������
{
	int i, choice;
	printf("What would you like to know:\nAll info about books (print 1)\nNewest and Oldest books (2)\nAll info sorted by lastname of authors (3)\n\n");
	scanf("%d", &choice);
	if (choice < 1 || choice > 3) // �������� �� ������������ �����
	{
		printf("Input error! Try again\n");
		return 1;
	}
	switch (choice)
	{
	case 1:
		printf("\nAll info about books\n");
		for (i = 0; i < M; i++)
			printf("%s - ""%s"", %d\n", bookshelf[i].FIO, bookshelf[i].bookName, bookshelf[i].publicationYear);
		break;
	case 2:
		printOldestAndNewestBooks(bookshelf);
		break;
	case 3:
		printSortedByLastname(bookshelf);
		break;
	}
}