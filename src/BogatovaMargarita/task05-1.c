#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
FILE *fp;
struct BOOK
{
	char name[50];
	int year;
	char author[50];
};
int EnterStruct(int n, FILE *fp, struct BOOK *book)
{
	int i = 1, j = 0;
	char  string[256];
	while (fgets(string, 256, fp))
	{
		++n;
		book = (struct BOOK*)realloc(book, n * sizeof(struct BOOK));
		book[n - 1].year = 0;
		i = 1; 	j = 0;
		while (string[i] != '\"') book[n - 1].name[j++] = string[i++];
		book[n - 1].name[j] = '\0';
		++i; j = 0;
		while (string[i] < '1' || string[i] > '9') book[n - 1].author[j++] = string[i++];
		book[n - 1].author[j] = '\0';
		while (string[i] != '\n') book[n - 1].year = book[n - 1].year * 10 + string[i++] - '0';
	}
	return n;
}
void NewOld(int n, struct BOOK *book)
{
	int old = 0, new = 0, i;
	for (i = 0; i < n; ++i)
	{
		if (book[i].year > book[new].year)
			new = i;
		if (book[i].year < book[old].year)
			old = i;
	}
	printf("%s\t%s\t%i\n", book[old].name, book[old].author, book[old].year);
	printf("%s\t%s\t%i\n", book[new].name, book[new].author, book[new].year);
}
void SortAuthor(int n, struct BOOK *book)
{
	int temp, i ,j;
	struct BOOK temp2;
	for (i = 0; i < n - 1; i++)
	{
		for (j = i + 1; j < n; j++)
		{
			temp = strcmp(book[i].author, book[j].author);
			if (temp > 0)
			{
				temp2 = book[i];
				book[i] = book[j];
				book[j] = temp2;
			}
		}
	}
	for (i = 0; i<n; i++)   printf("%s\t%s\t%i\n", book[i].name, book[i].author, book[i].year);
}
int main()
{
	char *fname = "book.txt";
	int n = 0,i;
	struct BOOK *book = (struct BOOK*)malloc(sizeof(struct BOOK));
	if (book == NULL)
	{
		printf("Error\n");
		return(1);
	}
	fp = fopen(fname, "r");
	if (fp == NULL)
	{
		printf("Error\n");
		return(1);
	}
	n=EnterStruct(n, fp, book);
    for (i=0; i<n; i++)   printf("%s\t%s\t%i\n", book[i].name, book[i].author, book[i].year);
	NewOld(n,book);
	SortAuthor(n, book);
	free(book);
	fclose(fp);
	getchar();
	return 0;
}