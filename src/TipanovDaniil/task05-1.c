#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>

int temp[2]; //temp[0] - ����� ������ �����, temp[1] - ����� ����� �����

struct BOOK
{
	char name[256];
	char FIO[256];
	int year;
};

void readFile(FILE *fp, struct BOOK *mas)
{
	char buf[256];
	fgets(mas->name, 256, fp);
	fgets(mas->FIO, 256, fp);
	fgets(buf, 256, fp);
	mas->year = atoi(buf);
}

void print(struct BOOK *mas)
{
	printf("%s", mas->name);
	printf("%s", mas->FIO);
	printf("%d", mas->year);
	printf("\n\n");
}

void sortYear(int amount, struct BOOK *mas)
{
	int old = mas[0].year, new = mas[0].year;
	for (int i = 1; i < amount; i++)
	{
		if (mas[i].year < old)
		{
			old = mas[i].year;
			temp[0] = i;
		}
		if (mas[i].year > new)
		{
			new = mas[i].year;
			temp[1] = i;
		}
	}
}

void sortAuthors(int amount, struct BOOK *mas)
{
	int i, j;
	struct BOOK temp;
	for (i = 0; i < amount - 1; i++)
		for (j = amount - 1; j > i; j--)
			if (mas[j - 1].FIO < mas[j].FIO)
			{
				temp = mas[j - 1];
				mas[j - 1] = mas[j];
				mas[j] = temp;
			}
}

int main()
{
	FILE *fp;
	int i = 0, j = 0;
	struct BOOK *arr;
	arr = (struct BOOK*)malloc(100 * sizeof(struct BOOK));
	fp = fopen("Library.txt", "r");
	for (i = 0; fgetc(fp) != EOF; i++)
		readFile(fp, &arr[i]);
	fclose(fp);
	puts("The list of books:");
	while (j != i)
	{
		print(&arr[j]);
		j++;
	}
	j = 0;
	sortYear(i, arr);
	puts("The oldest book:");
	print(&arr[temp[0]]);
	puts("The newest book:");
	print(&arr[temp[1]]);
	sortAuthors(i, arr);
	puts("Now I'm gonna print sorted list!");
	while (j != i)
	{
		print(&arr[j]);
		j++;
	}
	free(arr);
	return 0;
}