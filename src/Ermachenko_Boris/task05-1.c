#define _CRT_SECURE_NO_WARNINGS
#include <Windows.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <conio.h>
#include <math.h>
FILE *file;
#define N 30
#define K 30
int main() {
	char buf[256];
	int maxi,mini,k,i = 0;
	int j = 0;
	int max= 0;
	int min = 0;
	struct BOOK {
		char name_book[N];
		char name_auth[K];
		int year;
	};
	struct BOOK* array = (struct BOOK*)malloc(sizeof(struct BOOK)*N);
	file = fopen("file.txt", "r");
	if (file == NULL) {
		perror("file.txt:");
		return 1;
	}
	while (fgets(buf, 256, file)) {
		printf("%s", buf);
		k = 0;
		while (buf[i] != ' ') {
			array[j].name_auth[k] = buf[i];
			i++;
			k++;
		}
		array[j].name_auth[k] = '\0';
		i++;
		k = 0;
		while (buf[i] != ' ') {
			array[j].name_book[k] = buf[i];
			i++;
			k++;
		}
		array[j].name_book[k] = '\0';
		i++;
		array[j].year = 0;
		while (buf[i] != '\n') {
			array[j].year = array[j].year * 10 + buf[i] - '0';
			i++;
		}
		if (max == 0) { max = array[j].year; maxi = j; }
		if (array[j].year >=max) {
			max = array[j].year;
			maxi = j;
		}
		if (min == 0) { min = array[j].year; mini = j; }
		if (array[j].year <= min) {
			min = array[j].year;
			mini = j;
		}
		j++;
		i = 0;
	}
	printf("\n new: %s, %s, %d\n",array[maxi].name_auth, array[maxi].name_book, array[maxi].year);
	printf("old: %s, %s, %d\n",array[mini].name_auth, array[mini].name_book, array[mini].year);
	printf("\n");
	for(i=0;i<26;i++)
		for(k=0;k<j;k++)
		if(array[k].name_auth[0]=='A'+i)
			printf("%s, %s, %d\n ", array[k].name_auth, array[k].name_book, array[k].year);
	fclose(file);
	return 0;
}