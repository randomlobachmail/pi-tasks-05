#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int n = 1;
int maxn, minn;

struct BOOK
{
    char bname[256];
    char fio[256];
    int date;
};

struct BOOK* readSBook(FILE *fp, struct BOOK *bp)
{
    char buf[256];
    
    while (fgets(bp[n-1].bname , 256, fp))
    {
        fgets(bp[n-1].fio, 256, fp);
        
        fgets(buf, 256, fp);
        bp[n-1].date = atoi(buf);
        
        n++;
        bp = (struct BOOK*)realloc(bp, n*sizeof(struct BOOK));
    }
    return bp;
}

void outSBook(struct BOOK* bp)
{
    for (int i = 0; i< n; i++)
        printf("%s%s\%d\n\n", bp[i].bname, bp[i].fio, bp[i].date);
}

void NewOld(struct BOOK* bp)
{
    int max = 1, min = 3000;
    
    for (int i = 0; i < n; i++)
    {
        if (bp[i].date > max)
        {
            max = bp[i].date;
            maxn = i;
        }
        if (bp[i].date < min && bp[i].date > 0)
        {
            min = bp[i].date;
            minn = i;
        }
    }
    printf ("Newest:\n%s%s%d\n\n", bp[maxn].bname, bp[maxn].fio, bp[maxn].date);
    printf ("Oldest:\n%s%s%d\n\n", bp[minn].bname, bp[minn].fio, bp[minn].date);
}

void SortByO(struct BOOK *bp)
{
    for (int i = 0; i < n -1; i++)
    {
        for (int j = 0; j < n - 1 - i; j++ )
        {
            if (strcmp(bp[j].fio, bp[j+1].fio) > 0)
            {
                struct BOOK t;
                t = bp[j+1];
                bp[j+1] = bp[j];
                bp[j] = t;
            }
        }
    }
    outSBook(bp);
}

int main()
{
    FILE *f;
    struct BOOK *book = (struct BOOK*)malloc(2*sizeof(struct BOOK));
    
    f = fopen("data.txt", "r");
    
    book = readSBook(f, book);
    outSBook(book);
    NewOld(book);
    SortByO(book);
        
    free(book);
    fclose(f);
    return 0;
}