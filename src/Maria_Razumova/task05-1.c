#include <stdio.h>
#include <string.h>
#include <malloc.h>

#define N 256
struct Book
{
    char *name;
    char *authorFio;
    int year;
};
void recordBook (struct Book* book, FILE*in)
{
    char str[N];
    int i,year;
    fgets(str,N,in);
    book->name = (char*)malloc((strlen(str)+1)*sizeof(char));
    for(i=0;i<=strlen(str);i++)
    {
        book->name[i]=str[i];
    }
    fgets(str,N,in);
    book->authorFio = (char*)malloc((strlen(str)+1)*sizeof(char));
    for(i=0;i<=strlen(str);i++)
    {
        book->authorFio[i]=str[i];
    }
    fscanf(in,"%d",&year);
    book->year=year;
    fseek(in,2,SEEK_CUR);
}
void deleteBooks(struct Book *books, int size)
{
    int i;
    for(i=0;i<size;i++)
    {
        free(books[i].authorFio);
        free(books[i].name);
    }
}
void printAboutBooks(struct Book *books, int size)
{
    int i;
    for (i=0;i<size;i++)
    {
       printf("author %s\n title %s\n year %d \n\n",books[i].authorFio,books[i].name,books[i].year);
    }
}
void printAuthorSort(struct Book *books, int size)
{
    int i;
    struct Book temp;
    int boo;
    do {
        boo=1;
        for (i=0;i<size-1;i++)
        {
            if (books[i].authorFio<books[i+1].authorFio)
            {
                boo=0;
                temp = books[i];
                books[i]=books[i+1];
                books[i]=temp;
            }
        }
    }while(boo=0);
    printAboutBooks;
}
void printOldestLatest(struct Book *books, int size)
{
    int i, iOldest, iLatest;
    int oldest = 2100;
    int latest = 0;
    for (i=0;i<size;i++)
    {
        if (books[i].year<oldest)
        {
            oldest = books[i].year;
            iOldest= i;
        }
        if (books[i].year>latest)
        {
            latest = books[i].year;
            iLatest = i;
        }
    }
    printf("Oldest:\n author %s\n title %s\n year %d \n\n",books[iOldest].authorFio,books[iOldest].name,books[iOldest].year);
    printf("Latest:\n author %s\n title %s\n year %d \n\n",books[iLatest].authorFio,books[iLatest].name,books[iLatest].year);
}
int main(void)
{
    int n=10,i=0,pos,end;
    FILE *file;
    struct Book* books;
    file = fopen("input.txt","r");
    if (file==NULL)
    {
        printf("Cannot open");
        return 1;
    }
    books = (struct Book*)malloc(n*sizeof(struct Book));
    fseek(file, 0, SEEK_END);
    end = ftell(file);
    fseek(file,0,SEEK_SET);
    pos = ftell(file);
    while (pos<end)
    {
        recordBook(books+i,file);

        i++;
        if (i==n-1)
        {
            n*=2;
            books=(struct Book*)malloc(n*sizeof(struct Book));
        }
        pos = ftell(file);
    }
    printAboutBooks(books,i);
    deleteBooks(books, i);
    free(books);

    return 0;
}